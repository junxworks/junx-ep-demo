package io.github.junxworks.ep.demo.modules.table.service.impl;

import java.util.Date;
import java.util.List;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import io.github.junxworks.ep.auth.model.UserModel;
import io.github.junxworks.ep.demo.modules.table.dto.StudentConditionDto;
import io.github.junxworks.ep.demo.modules.table.dto.StudentDto;
import io.github.junxworks.ep.demo.modules.table.entity.DStudent;
import io.github.junxworks.ep.demo.modules.table.mapper.TableDemoMapper;
import io.github.junxworks.ep.demo.modules.table.service.TableDemoService;
import io.github.junxworks.ep.demo.modules.table.vo.StudentVo;
import io.github.junxworks.ep.sys.constants.RecordStatus;

@Service
public class TableDemoServiceImpl implements TableDemoService {
	@Autowired
	private TableDemoMapper tableDemoMapper;

	@Override
	public List<StudentVo> queryStudentList(StudentConditionDto condition) {
		return tableDemoMapper.queryStudentListByCondition(condition);
	}

	@Override
	public StudentVo queryStudentById(Long id) {
		return tableDemoMapper.queryStudentById(id);
	}

	@Override
	public int saveStudent(StudentDto entity) {
		DStudent sdutent = new DStudent(); //entity属于模块私有资源，不建议直接作为dto使用
		BeanUtils.copyProperties(entity, sdutent);
		UserModel user = (UserModel) SecurityUtils.getSubject().getPrincipal();
		Long userId = user.getId();
		if (sdutent.getId() != null) {
			sdutent.setUpdateUser(userId);
			sdutent.setUpdateTime(new Date());
			return tableDemoMapper.updateWithoutNull(sdutent);
		} else {
			sdutent.setCreateUser(userId);
			sdutent.setCreateTime(new Date());
			sdutent.setStatus(RecordStatus.NORMAL.getValue());
			return tableDemoMapper.insertWithoutNull(sdutent);
		}
	}

	@Override
	public int deleteStudent(Long id) {
		DStudent sdutent = new DStudent();
		sdutent.setId(id);
		sdutent.setStatus(RecordStatus.DELETED.getValue());
		return tableDemoMapper.updateWithoutNull(sdutent);
	}

}
