/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  DataSourceConfig.java   
 * @Package io.github.junxworks.ep.example.config   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-1-24 15:35:52   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.ep.demo.config;

import javax.sql.DataSource;

import org.apache.ibatis.annotations.Mapper;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.alibaba.druid.spring.boot.autoconfigure.DruidDataSourceBuilder;

@Configuration
@MapperScan(basePackages = { "io.github.junxworks.ep.*" }, annotationClass = Mapper.class) //io.github.junxworks.ep是ep的mapper扫苗，还可以添加项目自身的mapper扫描，如com.xxx.ccc.mapper
public class DataSourceConfig {

	@Bean
	@ConfigurationProperties("spring.datasource.druid.main")
	DataSource mainDataSource() {
		return DruidDataSourceBuilder.create().build();
	}

}
