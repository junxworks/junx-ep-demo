package io.github.junxworks.ep.demo.modules.table.service;

import java.util.List;

import io.github.junxworks.ep.demo.modules.table.dto.StudentConditionDto;
import io.github.junxworks.ep.demo.modules.table.dto.StudentDto;
import io.github.junxworks.ep.demo.modules.table.vo.StudentVo;

public interface TableDemoService {

	/**
	 * 查询学生列表
	 *
	 * @param condition the condition
	 * @return the list
	 */
	List<StudentVo> queryStudentList(StudentConditionDto condition);

	StudentVo queryStudentById(Long id);

	int saveStudent(StudentDto entity);

	int deleteStudent(Long id);
}
