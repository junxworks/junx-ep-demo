/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  Config.java   
 * @Package io.github.junxworks.ep.example.config   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-1-24 15:35:52   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.ep.demo.config;

import java.util.Map;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import io.github.junxworks.ep.auth.RamConstants;
import io.github.junxworks.ep.core.fs.FsClient;
import io.github.junxworks.junx.event.EventBus;
import io.github.junxworks.junx.event.impl.SimpleEventBus;

@Configuration
public class Config {

	@Bean
	FilterRegistrationBean<CorsFilter> corsFilter() {
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		CorsConfiguration config = new CorsConfiguration();
		config.setAllowCredentials(true);
		config.setAllowedOriginPatterns(Lists.newArrayList("*"));
		config.addAllowedHeader("*");
		config.addAllowedMethod("*");
		source.registerCorsConfiguration("/**", config); // CORS 配置对所有接口都有效
		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<CorsFilter>(new CorsFilter(source));
		bean.setOrder(0);
		return bean;
	}
	
	@Bean(initMethod = "start", destroyMethod = "stop")
	EventBus eventBus() {
		SimpleEventBus eventBus = new SimpleEventBus();
		eventBus.setName("Global-Event-Bus");
		return eventBus;
	}

	@Bean(initMethod = "start", destroyMethod = "stop")
	FsClient fsClient() {
		FsClient fs = new FsClient("http://localhost:11111/demo", "demo");
		Map<String,String> headers=Maps.newHashMap();
		headers.put(RamConstants.RAM_HEADER_ACCESSKEY, "test");
		headers.put(RamConstants.RAM_HEADER_ACCESSSECRET, "123");
		fs.setHeaders(headers);
		return fs;
	}
	
	
}
