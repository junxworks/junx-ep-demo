/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  TableDemoMapper.java   
 * @Package io.github.junxworks.ep.demo.modules.table.mapper   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-3-29 21:54:05   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.ep.demo.modules.table.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import io.github.junxworks.ep.core.orm.BaseMapper;
import io.github.junxworks.ep.demo.modules.table.dto.StudentConditionDto;
import io.github.junxworks.ep.demo.modules.table.vo.StudentVo;

/**
 * {类的详细说明}.
 *
 * @ClassName:  TableDemoMapper
 * @author: Michael
 * @date:   2021-3-29 21:54:05
 * @since:  v1.0
 */
@Mapper
public interface TableDemoMapper extends BaseMapper {

	/**
	 * Query student by id.
	 *
	 * @param id the id
	 * @return the student vo
	 */
	@Select("select * from d_student where id=#{id}")
	StudentVo queryStudentById(Long id);

	/**
	 * Query student list by condition.
	 *
	 * @param condition the condition
	 * @return the list
	 */
	List<StudentVo> queryStudentListByCondition(StudentConditionDto condition);
}
