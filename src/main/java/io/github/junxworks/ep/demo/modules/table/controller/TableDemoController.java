package io.github.junxworks.ep.demo.modules.table.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;

import io.github.junxworks.ep.core.Result;
import io.github.junxworks.ep.core.utils.PageUtils;
import io.github.junxworks.ep.demo.modules.table.dto.StudentConditionDto;
import io.github.junxworks.ep.demo.modules.table.dto.StudentDto;
import io.github.junxworks.ep.demo.modules.table.service.TableDemoService;
import io.github.junxworks.ep.sys.annotations.EpLog;

@RestController
@RequestMapping("/demo/table")
public class TableDemoController {
	@Autowired
	private TableDemoService tableDemoService;

	/**
	 * 查询学生列表
	 *
	 * @param condition the condition
	 * @return the result
	 */
	@GetMapping("/students")
	public Result queryStudentList(StudentConditionDto condition) {
		PageUtils.setPage(condition);//建议在service外层设置分页参数，这样查询列表的方法可以重用，例如在excel导出时候不需要分页
		return Result.ok(new PageInfo<>(tableDemoService.queryStudentList(condition)));
	}

	/**
	 * 保存学生信息
	 *
	 * @param entity the entity
	 * @return the result
	 */
	@EpLog("保存学生信息")
	@PostMapping("/students")
	public Result saveStudent(@RequestBody StudentDto entity) {
		return Result.ok(tableDemoService.saveStudent(entity));
	}

	/**
	 * 根据ID查询学生信息
	 *
	 * @param id the id
	 * @return the result
	 */
	@GetMapping("/students/{id}")
	public Result queryStudentById(@PathVariable("id") Long id) {
		return Result.ok(tableDemoService.queryStudentById(id));
	}

	/**
	 * 软删除学生
	 *
	 * @param id the id
	 * @return the result
	 */
	@EpLog("删除学生信息")
	@DeleteMapping("/students/{id}")
	public Result deleteStudentById(@PathVariable("id") Long id) {
		return Result.ok(tableDemoService.deleteStudent(id));
	}
}
