/*
 ***************************************************************************************
 * EP for web developers.Supported By Junxworks
 * @Title:  JunxEpDemoApplication.java   
 * @Package io.github.junxworks.ep.example   
 * @Description: (用一句话描述该文件做什么)   
 * @author: Administrator
 * @date:   2021-1-24 15:35:51   
 * @version V1.0 
 * @Copyright: 2021 Junxworks. All rights reserved. 
 * 注意：
 *  ---------------------------------------------------------------------------------- 
 * 文件修改记录
 *     文件版本：         修改人：             修改原因：
 ***************************************************************************************
 */
package io.github.junxworks.ep.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;

import io.github.junxworks.ep.core.EnableGlobalExceptionHandler;
import io.github.junxworks.ep.core.security.access.EnableAccessLog;
import io.github.junxworks.ep.core.security.sql.EnableSQLFilter;
import io.github.junxworks.ep.core.utils.IPUtils;
import io.github.junxworks.ep.sys.annotations.EnableEPSys;
import io.github.junxworks.junx.core.exception.BaseRuntimeException;

@EnableAccessLog //开启访问日志
@EnableSQLFilter //开启sql注入过滤
@EnableGlobalExceptionHandler //开启全局异常捕获
@EnableEPSys //开启EP基础系统，含人员管理、菜单管理等基础功能，其他模块注解要在这个注解下面
@SpringBootApplication(exclude = { DataSourceAutoConfiguration.class })
public class Application {
	static {
		try {
			IPUtils.initializeServerIP();//如果ip合法，则把IP设置到系统环境变量中，注册到zookeeper的时候会使用到
		} catch (Exception e) {
			throw new BaseRuntimeException(e);
		}
	}

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

}
