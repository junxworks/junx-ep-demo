package io.github.junxworks.ep.demo.modules.table.dto;

import io.github.junxworks.ep.core.Pageable;

public class StudentConditionDto extends Pageable {

	private String studentName;

	private String studentNo;

	private Byte status;

	public String getStudentName() {
		return studentName;
	}

	public void setStudentName(String studentName) {
		this.studentName = studentName;
	}

	public String getStudentNo() {
		return studentNo;
	}

	public void setStudentNo(String studentNo) {
		this.studentNo = studentNo;
	}

	public Byte getStatus() {
		return status;
	}

	public void setStatus(Byte status) {
		this.status = status;
	}

}