layui.use([ 'upload'], function() {
	var upload = layui.upload;
	//字典上传
	var url=appendCtx('/ep/files');
	//var url=appendCtx('/ep/fs/files');
	//var url='http://localhost:11000/ep/fs/files';
	upload.render({
		elem: '#uploadDicts'
		, url: url //此处用的是第三方的 http 请求演示，实际使用时改成您自己的上传接口即可。
		, accept: "file"
		, exts: "xlsx|txt"
		, before: function(obj) { //obj参数包含的信息，跟 choose回调完全一致，可参见上文。
			layer.load();
		}
		, done: function(res) {
			layer.closeAll('loading');
			if (res.ok) {
				layer.msg('上传成功');
			} else {
  		        layer.alert(res.msg, {icon: 2});
			}
		}
		, error: function() {
			layer.closeAll('loading');
		}
	});
});