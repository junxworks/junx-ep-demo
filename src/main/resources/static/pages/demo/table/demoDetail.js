layui.use(['form','laydate'], function () {
    var form = layui.form;
    var laydate = layui.laydate;
    var id = getParam("id"); //获取ID入参
    laydate.render({
        elem: '#birthday' //指定元素
    });
    dict2Select("studentStatus","status");
    form.render();
    if (!isNull(id)) {
    	io.get('/demo/table/students/' + id,function(res){
    		form.val('editForm',res.data);
    	});
    }
    //监听提交
    form.on('submit(save)', function (formData) {
    	var data = JSON.stringify(formData.field);
		io.post("/demo/table/students",data,function(res){
            //关闭当前页
            setTimeout(function(){return closePage();},500);
            //刷新列表
            window.parent.refreshTableData();
		});
        return false;
    });
});

