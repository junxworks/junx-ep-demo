var tableIns;//表格实例
layui.use(['form','table'], function(){
    var form = layui.form;
    var table = layui.table;
    dict2Select("studentStatus","status");//将数据字典中的sdudentStatus，映射到id=status的元素中
    form.render();//渲染form表单
    //渲染table
    tableIns = table.render({
        elem: '#listTable'
        , url: appendCtx("/demo/table/students")
        , even: true
        , cols: [[
            {field: 'id', title: 'ID', align: 'center',width: 80},
            {field: 'studentName', title: '姓名', align: 'center',width: 100},
            {field: 'studentNo', title: '学号', align: 'center',width: 100},
            {field: 'birthday', title: '生日', align: 'center',width: 140},
            {field: 'status', title: '状态', align: 'center',width: 100,templet:
            	function(d)
            	{
            		return translate("studentStatus",d.status);
            	}
            },
            {title: '操作', align: 'center',width: 200,fixed:"right",toolbar: '#operBar'}
        ]]
    	, page: true
        , request: {
            pageName: 'pageNo' //页码的参数名称，默认：page
            , limitName: 'pageSize' //每页数据量的参数名，默认：limit
        }
        , parseData: function (res) { //res 即为原始返回的数据
            return {
                "code": res.code, //解析接口状态
                "msg": res.msg, //解析提示文本
                "count": res.data.total, //解析数据长度
                "data": res.data.list //解析数据列表
                ,"attr":res.attr
            };
        }
    });
    form.on('submit(search)', function(formData){
        tableIns.reload({
            where:formData.field,
            page:{
                curr: 1
            }
        });
        return false;
    });
    
    form.on('submit(add)', function(){
    	layer.open({
            type: 2,
            title: '新增学霸',
            area: ['500px', '600px'],
            content: 'demoDetail.html',
        });
        return false;
    });
});

function edit(id) {
    layer.open({
        type: 2,
        title: '修改学霸',
        area: ['500px', '600px'],
        content: 'demoDetail.html?id=' + id,
    });
}

function refreshTableData() {
    tableIns.reload({});
}

function del(id){
	io.delete("/demo/table/students/"+id,function(res){
		refreshTableData();
	});
}
